const mongoose = require("mongoose");

const instituteSchema = new mongoose.Schema({
    "name": {
        "type": "String",
        "require":true
    },
    "address": {
        type:String,
        require:true
    },
    "mobile": {
        "type": "String"
    },
    "director": {
        "type": "String"
    }


}, { collection: 'institutions' })

module.exports = mongoose.model('teacher', instituteSchema)