const mongoose = require('mongoose')
const { strict } = require('assert')
const { stringify } = require('querystring')

const questionSchema = new mongoose.Schema({
    "question": {
        type: String,
        required: true
    },
    "answer1": {
        type: String,
        required: true
    },
    "answer2": {
        type: String,
        required: true
    },
    "answer3": {
        type: String,
        required: true
    },
    "answer4": {
        type: String,
        required: true
    },
    "correctAnswer": {
        type: String,
        required: true
    }
})

const exampaperSchema = new mongoose.Schema({
    "term": {
        type: Number,
        required: true
    },
    "subject": {
        type: String,
        required: true
    },
    "grade": {
        type: Number,
        required: true
    },
    "medium": {
        type: String,
        required: true
    },
    "instituteName": {
        type: String,
        required: true
    },
    "questions":{
        type:[questionSchema]
    }
}, { collection: 'exampaper' })

module.exports = mongoose.model('exampaper', exampaperSchema)