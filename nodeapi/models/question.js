const mongoose = require('mongoose')
const { strict } = require('assert')
const { stringify } = require('querystring')

const questionSchema = new mongoose.Schema({
    "question": {
        type: String,
        required: true
    },
    "answer1": {
        type: String,
        required: true
    },
    "answer2": {
        type: String,
        required: true
    },
    "answer3": {
        type: String,
        required: true
    },
    "answer4": {
        type: String,
        required: true
    },
    "correctAnswer": {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('question', questionSchema)