// imports
const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const path = require('path')
const db = require('./config/db');
const app = express();
const bodyParser = require('body-parser')
const institution = require('./app/routes/institution_routes')
const exam_papers = require('./app/routes/exampaper_routes')

// app.use(bodyParser.raw());
//Multer storage engin
const storage = multer.diskStorage({
    destination:'./public/uploads',
    filename:function(req,file,cb){
        cb(nul , file.fieldname +'-'+ Date.now()+ path.extname(file.originalname))
    }
})

//Init upload
const upload = multer({
    storage:storage
}).single();

// DatabaseConnection
const port =2003;
mongoose.connect(db.url,{useNewUrlParser: true})
mongoose.set('useFindAndModify', false);
const con = mongoose.connection;
app.use(express.json())

con.on('open',()=>{
    console.log('Database connected');
})

// Routes Link
app.use('/institutes', institution)
app.use('/exam_papers', exam_papers)

app.listen(port,()=>{
    console.log('Server Connected ' +port);
})