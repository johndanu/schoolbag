const express = require('express');
const { model } = require('mongoose');
const { ReplSet } = require('mongodb');
const router = express.Router()
const Exampaper = require('../../models/exampaper');
const exampaper = require('../../models/exampaper');
const Question = require('../../models/question')

router.get('/', async (req, res) => {
    try {
        const exampaper = await Exampaper.find()
        res.json(exampaper)
    } catch (err) {
        console.log(err);
    }
})

router.post('/', (req, res) => {
    const exampaper = new Exampaper({
        term: req.body.term,
        subject: req.body.subject,
        grade: req.body.grade,
        medium: req.body.medium,
        instituteName: req.body.instituteName
    })
    try {
        const exampaper1 = exampaper.save((err, post) => {
            res.json(post)
        })
    } catch (error) {
        console.log("Error " + err);
    }
})

router.get('/:id', async (req, res) => {
    let id = req.params.id;
    Exampaper.findOne({ _id: id }, (err, doc) => {
        res.json(doc)
    })
})

router.patch('/:id', (req, res) => {

    let question1 = new Question({
        question: req.body.question,
        answer1: req.body.answer1,
        answer2: req.body.answer2,
        answer3: req.body.answer3,
        answer4: req.body.answer4,
        correctAnswer: req.body.correctAnswer
    })
    const id = req.params.id
    try {
        const exampaper1 = Exampaper.findOneAndUpdate({ '_id': id }, { $push: { "questions": question1 } }, (err, doc) => {
            if (err) {
                res.send(err);
                console.log("pushed");
            } else {
                res.json(doc)
            }
        })
    } catch (err) {
        console.log(err);
    }
})

//Adding Question

// router.route("/addQuestion/:id").put((req, res) => {
//     const id = req.params.id;
//     let question1 = new question({
//         quesion: req.body.question,
//         answer1: req.body.answer1,
//         answer2: req.body.answer2,
//         answer3: req.body.answer3,
//         answer4: req.body.answer4,
//         correctAnswer: req.body.correctAnswer
//     })
//     const exampaperQuest = exampaper.updateOne({
//         _id: id
//     },
//         {
//             $push:{question:[question1]}
// },(err,res)=>{
//     if (err) {
//         res.send(err);
//     } else {
//         res.json(doc)
//     }
// })
// })

module.exports = router