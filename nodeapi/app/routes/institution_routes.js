const express = require('express');
const { model } = require('mongoose');
const { ReplSet } = require('mongodb');
const router = express.Router()
const Institution = require('../../models/institution');


router.get('/:id', async (req, res) => {
    try {
        let id = req.params.id;
        const institution = await Institution.findOne({ '_id': id })
        res.json(institution)
    } catch (err) {
        res.send('Error' + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const institution = await Institution.find()
        res.json(institution)
    } catch (err) {
        res.send('Error' + err)
    }
})

router.delete('/:id', function (req, res, next) {
    console.log(req.params.id);
    Institution.findByIdAndRemove(req.params.id, (err, post) => {
        if (err) return next(err);
        res.json(post);
    });
});


router.post('/', async (req, res) => {
    const institution = new Institution({
        name: req.body.name,
        mobile: req.body.mobile,
        address:req.body.address,
        director: req.body.director
    })
    try {
        const institution1 = await institution.save((err,post) => {
            console.log('error = ' +err);
            res.json(post);
        })
    } catch (err) {
        res.send('Error' + err)
    }
})

module.exports = router