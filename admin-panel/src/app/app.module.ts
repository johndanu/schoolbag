import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPaperComponent } from './add-paper/add-paper.component';
import { ListPaperComponent } from './add-paper/list-paper/list-paper.component';
import { AddPaperFormComponent } from './add-paper/add-paper-form/add-paper-form.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPaperComponent,
    ListPaperComponent,
    AddPaperFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
